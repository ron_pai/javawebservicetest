package BusinessLogic;

import model.UrgentRequest;

import javax.jws.*;
import java.util.List;

@WebService(name = "TestProgram", targetNamespace = "http://test.com/")
public interface IUrgentIssuesBL {

    @WebMethod
    int submitUrgentRequest(@WebParam(name = "request")String request,
                                @WebParam(name = "priority")int priority);

    @WebMethod
    List<UrgentRequest> getUrgentRequests();

    @WebMethod
    UrgentRequest getUrgentRequest(@WebParam(name = "id")int id);

    @WebMethod
    boolean completeRequest(@WebParam(name = "id")int id);
}
