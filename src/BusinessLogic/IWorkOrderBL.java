package BusinessLogic;

import model.WorkOrder;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * Created by rpai on 7/18/2018.
 */
@WebService(name = "TestProgram", targetNamespace = "http://test.com/")
public interface IWorkOrderBL {
    @WebMethod
    int insertWorkOrder(@WebParam(name = "name") String name,
                        @WebParam(name = "description") String description,
                        @WebParam(name = "username") String userName);

    @WebMethod
    WorkOrder getWorkOrder(@WebParam(name = "id")int id);

    @WebMethod
    int updateWorkOrder(@WebParam(name = "id") int id,
                        @WebParam(name = "name") String name,
                        @WebParam(name = "description") String description,
                        @WebParam(name = "username") String userName,
                        @WebParam(name = "iscompleted") boolean isCompleted);

    @WebMethod
    List<WorkOrder> getWorkOrders();
}
