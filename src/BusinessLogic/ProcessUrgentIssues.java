package BusinessLogic;

import model.UrgentRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class ProcessUrgentIssues extends Thread{

    private PriorityQueue<UrgentRequest> queue = null;
    private List<UrgentRequest> allRequests = new ArrayList<>();

    private boolean isDone = false;

    /**
     * Default initialization
     */
    public ProcessUrgentIssues(){
        this.queue = new PriorityQueue<>();
        this.start();
    }

    /**
     * Running thread to process an issue a minute.
     */
    @Override
    public void run(){
        while(!isDone){
            if(this.queue.size() > 0)
            {
                synchronized (this.queue) {
                    UrgentRequest request = queue.poll();
                    this.setComplete(request);
                }
            }

            try
            {
                Thread.sleep(1000 * 60);
            }
            catch(Exception e){

            }
        }
    }

    /**
     * Process a request manually
     * @param request
     */
    public void setComplete(UrgentRequest request) {
          request.setComplete();
          synchronized (this.queue) {
              if (this.queue.contains(request)) {
                  this.queue.remove(request);
              }
          }
        System.out.println("Completed: " + request.toString());
    }

    /**
     * get a specific request
     * @param id
     * @return
     */
    public UrgentRequest getRequest(int id){
        synchronized(allRequests) {
            for (UrgentRequest request : allRequests) {
                if (request.getId() == id) {
                    return request;
                }
            }
        }

        return null;
    }

    /**
     * return all requests
     * @return
     */
    public List<UrgentRequest> getAllRequests(){
        return allRequests;
    }

    public void addRequest(UrgentRequest request){
        synchronized (allRequests){
            this.allRequests.add(request);
        }
        synchronized (queue){
            this.queue.add(request);
        }
        System.out.println("Added: " + request.toString());
    }

    /**
     * Stop the thread
     */
    public void setDone(){
        this.isDone = true;
        this.interrupt();
    }
}
