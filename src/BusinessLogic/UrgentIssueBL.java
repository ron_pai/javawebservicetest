package BusinessLogic;

import model.UrgentRequest;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(endpointInterface = "BusinessLogic.IUrgentIssuesBL", targetNamespace = "http://test.com/")
public class UrgentIssueBL implements IUrgentIssuesBL{

    /**
     * the processor thread
     */
    private ProcessUrgentIssues processor;

    /**
     * initialization
     */
    public UrgentIssueBL(){
        processor = new ProcessUrgentIssues();
    }

    /**
     * submit a request into the processor
     * @param requestName request name
     * @param priority priority
     * @return id of request
     */
    @Override
    public int submitUrgentRequest(String requestName, int priority) {
        UrgentRequest request = new UrgentRequest(requestName, priority);
        processor.addRequest(request);
        return request.getId();
    }

    /**
     * retrieves a list of all requests
     * @return
     */
    @Override
    public List<UrgentRequest> getUrgentRequests() {
        return processor.getAllRequests();
    }

    /**
     * retrieve a request based on id
     * @param id
     * @return
     */
    @Override
    public UrgentRequest getUrgentRequest(int id) {
        return processor.getRequest(id);
    }

    /**
     * Set a request to be completed manually
     * @param id
     * @return
     */
    @Override
    public boolean completeRequest(int id) {
        UrgentRequest request = processor.getRequest(id);
        if(request != null) {
            processor.setComplete(request);
            return true;
        }
        return false;
    }
}
