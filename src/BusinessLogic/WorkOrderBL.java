package BusinessLogic;

import Database.DBWorkOrder;
import model.WorkOrder;

import javax.jws.WebService;
import java.util.*;

/**
 * Created by rpai on 7/18/2018.
 */
@WebService(endpointInterface = "BusinessLogic.IWorkOrderBL", targetNamespace = "http://test.com/")
public class WorkOrderBL implements IWorkOrderBL {

    /**
     * a lock for the database
     */
    private static Object dbLock = new Object();

    /**
     * get work order based on id
     * @param id
     * @return
     */
    public WorkOrder getWorkOrder(int id){
        System.out.println("Getting order #" + id);

        DBWorkOrder db = new DBWorkOrder();
        synchronized (dbLock) {
            return db.select(id);
        }
    }

    /**
     * Update work order based on id
     * @param id
     * @param name
     * @param description
     * @param userName
     * @param isCompleted
     * @return
     */
    @Override
    public int updateWorkOrder(int id, String name, String description, String userName, boolean isCompleted) {


        WorkOrder order = new WorkOrder(id, name, description, userName, isCompleted);
        System.out.println("Updating order #" + order.getTicketNumber());

        DBWorkOrder workOrder = new DBWorkOrder();
        synchronized (dbLock) {
            return workOrder.update(order);
        }
    }

    /**
     * get all work orders
     * @return
     */
    @Override
    public List<WorkOrder> getWorkOrders() {
        System.out.println("Getting all orders");
        DBWorkOrder db = new DBWorkOrder();
        synchronized (dbLock) {
            return db.selectall();
        }
    }

    /**
     * Insert the work order
     * @param name name
     * @param description description
     * @param username username
     * @return id
     */
    public int insertWorkOrder(String name, String description, String username){
        WorkOrder order = new WorkOrder(name, description, username);
        System.out.println("Inserting order " + order.getName());

        DBWorkOrder db = new DBWorkOrder();
        synchronized (dbLock) {
            return db.insert(order);
        }
    }
}
