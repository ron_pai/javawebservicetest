package Database;
import model.WorkOrder;

import java.sql.*;

public class DBConnection {

    String name = "";

    /**
     * default Constructor
     */
    public DBConnection()
    {
        this.name = "test";
    }

    /**
     * Initialize DBConnection
     * @param databaseName name of database
     */
    public DBConnection(String databaseName)
    {
        this.name = databaseName;
    }

    /**
     * Create the connection to the database
     * @return
     * @throws Exception
     */
    public Connection getConnection() throws Exception
    {
        Connection c = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:"+ this.name +".db");

        return c;
    }

    /**
     * Create the database
     * @return
     */
    public boolean createDatabase(){


        try {
            Connection c = getConnection();
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
