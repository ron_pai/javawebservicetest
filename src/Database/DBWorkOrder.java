package Database;

import com.sun.corba.se.spi.orbutil.threadpool.Work;
import model.WorkOrder;

import java.sql.*;
import java.util.*;

public class DBWorkOrder extends DBConnection{
    /**
     * Create a WorkOrder database table
     * @return success flag
     */
    public boolean createTable(){
        StringBuilder query = new StringBuilder();
        query.append("CREATE TABLE IF NOT EXISTS WorkOrder ");
        query.append("(ID INTEGER PRIMARY KEY NOT NULL, Name CHAR(50) NOT NULL, Description TEXT NOT NULL, Username CHAR(20) NOT NULL, SubmissionDate TEXT NOT NULL, IsCompleted INT NOT NULL)");

        try {
            Connection c = getConnection();

            Statement stmt = c.createStatement();
            stmt.executeUpdate(query.toString());
            stmt.close();
            c.close();
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * return a list of all work orders
     * @return
     */
    public List<WorkOrder> selectall(){
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM WorkOrder");

        try {
            Connection c = getConnection();

            PreparedStatement stmt = c.prepareStatement(query.toString());
            ResultSet result = stmt.executeQuery();
            List<WorkOrder> list = new ArrayList<WorkOrder>();
            while(result.next())
            {
                WorkOrder order = new WorkOrder(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("username"),
                        result.getBoolean("isCompleted")
                );
                list.add(order);
            }

            stmt.close();
            c.close();
            return list;
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    /**
     * get a work order based on id
     * @param id
     * @return
     */
    public WorkOrder select(int id){
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM WorkOrder WHERE ID = ?");

        try {
            Connection c = getConnection();

            PreparedStatement stmt = c.prepareStatement(query.toString());
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            WorkOrder order = null;
            if (result.next())
            {
                order = new WorkOrder(
                        result.getInt("id"),
                        result.getString("name"),
                        result.getString("description"),
                        result.getString("username"),
                        result.getBoolean("isCompleted")
                );
            }

            stmt.close();
            c.close();
            return order;
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Insert a work order
     * @param workOrder work order to insert
     * @return id in database
     */
    public int insert(WorkOrder workOrder)
    {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO WorkOrder (Name, Description, Username, SubmissionDate, IsCompleted) VALUES ");
        query.append("(?, ?, ?, ?, 0); ");

        String[] generatedColumns = {"ID"};
        try {
            Connection c = getConnection();

            PreparedStatement stmt = c.prepareStatement(query.toString(), generatedColumns);
            stmt.setString(1, workOrder.getName());
            stmt.setString(2, workOrder.getDescription());
            stmt.setString(3, workOrder.getUserName());
            stmt.setString(4, workOrder.getSubmissionDate().toString());
            stmt.execute();

            int id;
            try(ResultSet generatedKeys = stmt.getGeneratedKeys())
            {
                id = generatedKeys.getInt(1);
            }

            stmt.close();
            c.close();

            return id;
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * update a work order based on id
     * @param workOrder
     * @return
     */
    public int update(WorkOrder workOrder){
        StringBuilder query = new StringBuilder();
        query.append("UPDATE WorkOrder SET Name = ?, Description = ?, Username = ?, IsCompleted = ? WHERE ID = ? ");

        Connection c = null;
        PreparedStatement stmt = null;
        try {
            c = getConnection();

            stmt = c.prepareStatement(query.toString());
            stmt.setString(1, workOrder.getName());
            stmt.setString(2, workOrder.getDescription());
            stmt.setString(3, workOrder.getUserName());
            stmt.setBoolean(4, workOrder.isCompleted());
            stmt.setInt(5, workOrder.getTicketNumber());
            return stmt.executeUpdate();
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            return -1;
        } finally {

            try {
                if (stmt != null) {
                    stmt.close();
                }

                if (c != null) {
                    c.close();
                }
            }catch(Exception e){

            }
        }
    }
}
