package Endpoints;

import BusinessLogic.UrgentIssueBL;
import BusinessLogic.WorkOrderBL;

import javax.xml.ws.Endpoint;

/**
 * Created by rpai on 7/18/2018.
 */
public class ControllerPublisher {
    public static void main(String[] args) {
        if(args.length == 0) {
            Endpoint.publish("http://localhost:9999/ws/workOrder", new WorkOrderBL());
            Endpoint.publish("http://localhost:9999/ws/urgentRequest", new UrgentIssueBL());
        }
        else if(args.length == 2 && args[0].equals("-D"))
        {
            if(args[1].endsWith("/"))
            {
                args[1] = args[1].substring(0, args[1].length() - 1);
            }
            Endpoint.publish("http://"+ args[1] + "/ws/workOrder", new WorkOrderBL());
            Endpoint.publish("http://"+ args[1] + "/ws/urgentRequest", new UrgentIssueBL());
        }
        else{
            System.out.println("Invalid options. Use -D, then enter in TCP/IP Address and port. Ex: 0.0.0.0:9999");
        }
    }

}
