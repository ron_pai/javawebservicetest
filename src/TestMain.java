import Database.DBConnection;
import Database.DBWorkOrder;
import model.WorkOrder;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.List;

/**
 * Created by rpai on 7/18/2018.
 */
public class TestMain {
    public static void main(String[] args) throws Exception {

        URL url = new URL("http://localhost:9999/ws/workOrder?wsdl");

        //1st argument service URI, refer to wsdl document above
        //2nd argument is service name, refer to wsdl document above
        QName qname = new QName("http://test.com/", "WorkOrderBLService");

        Service service = Service.create(url, qname);

        BusinessLogic.IWorkOrderBL workOrderBL = service.getPort(BusinessLogic.IWorkOrderBL.class);
        List<WorkOrder> list = workOrderBL.getWorkOrders();
        for(WorkOrder order : list)
        {
            System.out.println(order);
        }


    }
}
