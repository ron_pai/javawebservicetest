package model;

import BusinessLogic.UrgentIssueBL;

import java.util.Date;

public class UrgentRequest extends Object implements Comparable<UrgentRequest> {
    private static int idCounter = 1;
    private int id;
    private String name;
    private int priority;
    private Date submitDate;
    private Date completionDate;

    /**
     * Get the id
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * get the priority
     * @return
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Submit date
     * @return
     */
    public Date getSubmitDate() {
        return submitDate;
    }

    /**
     * completion date
     * @return
     */
    public Date getCompletionDate() {
        return completionDate;
    }

    /**
     * default initialization
     * @param name
     * @param priority
     */
    public UrgentRequest(String name, int priority)
    {
        this.submitDate = new Date();
        this.completionDate = null;
        this.name = name;
        this.priority = priority;
        this.id = UrgentRequest.idCounter++;
    }

    /**
     * set the request to be completed
     */
    public void setComplete(){
        completionDate = new Date();
    }

    /**
     * Comparison for priority queue
     * @param o
     * @return
     */
    @Override
    public int compareTo(UrgentRequest o) {
        return o.getPriority() - this.priority;
    }

    /**
     * toString
     * @return
     */
    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("ID: " + this.id);
        str.append(" Priority: " + this.priority);
        str.append(" SubmissionDate: " + this.submitDate);
        str.append(" CompletionDate: " + this.completionDate);

        return str.toString();
    }
}
