package model;

import java.util.Date;

/**
 * Class to represent a work order form
 */
public class WorkOrder extends Object {
    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    private int ticketNumber;
    private String name;
    private String description;
    private String userName;
    private String completedBy;
    private Date submissionDate;
    private boolean isCompleted;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public Date getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(Date submissionDate) {
        this.submissionDate = submissionDate;
    }

    public WorkOrder() {
        this.isCompleted = false;
        this.completedBy = null;
        this.ticketNumber = -1;
        this.name = "";
        this.description = "";
        this.userName = "";
        this.submissionDate = new Date();
    }

    public WorkOrder(String name, String description, String userName) {
        this.name = name;
        this.description = description;
        this.userName = userName;
        this.submissionDate = new Date();
    }

    public WorkOrder(int ticketNumber, String name, String description, String userName, boolean isCompleted) {
        this(name, description, userName);
        this.ticketNumber = ticketNumber;
        this.isCompleted = isCompleted;
    }

    public String toString(){
        StringBuilder str = new StringBuilder();
        str.append("ID: " + this.ticketNumber);
        str.append(" SubmissionDate: " + this.submissionDate);

        return str.toString();
    }
}
